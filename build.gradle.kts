val nexusUsername: String by project
val nexusPassword: String by project

plugins {
    kotlin("jvm") version "1.7.21"
    kotlin("plugin.spring") version "1.7.21"
    id("org.springframework.boot") version "3.0.0"
    id("org.graalvm.buildtools.native") version "0.9.18"
}

group = "fr.vcy"

repositories {
    maven { url = uri("https://repo.spring.io/milestone") }
    mavenCentral()
}

dependencies {
    implementation(platform(org.springframework.boot.gradle.plugin.SpringBootPlugin.BOM_COORDINATES))
    implementation(platform("org.springframework.shell:spring-shell-dependencies:3.0.0-M3"))
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))
    implementation("org.springframework.shell:spring-shell-starter")
    implementation("org.springframework:spring-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-jooq")
    implementation("org.xerial:sqlite-jdbc")
    implementation("io.github.microutils:kotlin-logging-jvm:3.0.4")
    runtimeOnly("ch.qos.logback:logback-classic")
    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.mockito.kotlin:mockito-kotlin:4.1.0")
}

java.sourceCompatibility = JavaVersion.VERSION_17
tasks.compileKotlin {
    kotlinOptions.jvmTarget = "17"
}

tasks.test {
    useJUnitPlatform()
}

springBoot {
    buildInfo()
}

graalvmNative {
    binaries.all {
        resources.autodetect()
        javaLauncher.set(javaToolchains.launcherFor {
            languageVersion.set(JavaLanguageVersion.of(17))
        })
    }
    toolchainDetection.set(false)
}
