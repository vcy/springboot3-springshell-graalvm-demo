# Springboot3 - springshell - graalvm native - demo

On essaye en vrac [spring-boot3](https://spring.io/blog/2022/11/24/spring-boot-3-0-goes-ga), [spring-shell3](https://docs.spring.io/spring-shell/docs/3.0.0-M3/docs/), [sqlite](https://www.sqlite.org) avec [l'init à la flyway sans flyway de spring](https://docs.spring.io/spring-boot/docs/current/reference/html/howto.html#howto.data-initialization.using-basic-sql-scripts) 
et de la compile native avec le plugin gradle [graalvm](https://www.graalvm.org/)

### Build & tests

`$ ./gradlew build`

### Run the demo app

`$ java -jar build/libs/demo.jar`

### GraalVM native compile

Prérequis : graalvm install (le plus simple c'est via [sdkman](https://sdkman.io/))

`$ ./gradlew nativeCompile`  
`$ ./build/native/nativeCompile/demo`

### GraalVM native compile pour des OS différents du votre

Pour compiler un executable natif qui tourne sur Debian buster :  
`$ docker build -t graalvm-debian-buster .`  
`$ docker run -v "$(pwd):/app" graalvm-debian-buster bash -c "cd /app && ./gradlew nativeBuild"`  

Attention, certains fichiers et dossiers volumés sont créés par root, il faut peut être faire un sudo chown vous-meme derrière 
