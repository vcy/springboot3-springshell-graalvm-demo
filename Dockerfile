FROM debian:buster

ENV JAVA_VERSION=17
ENV GRAALVM_VERSION=22.3.0
ENV GRAALVM_ARCHIVE=graalvm-ce-java$JAVA_VERSION-linux-amd64-$GRAALVM_VERSION.tar.gz

RUN apt update -y && apt install -y curl build-essential libz-dev

RUN curl -L https://github.com/graalvm/graalvm-ce-builds/releases/download/vm-$GRAALVM_VERSION/$GRAALVM_ARCHIVE -o $GRAALVM_ARCHIVE
RUN mkdir -p /graalvm
RUN tar -vxzf $GRAALVM_ARCHIVE -C /graalvm

ENV PATH=/graalvm/graalvm-ce-java$JAVA_VERSION-$GRAALVM_VERSION/bin:$PATH
ENV JAVA_HOME=/graalvm/graalvm-ce-java$JAVA_VERSION-$GRAALVM_VERSION

RUN java -version
