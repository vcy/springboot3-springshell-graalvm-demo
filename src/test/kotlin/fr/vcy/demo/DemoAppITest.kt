package fr.vcy.demo

import mu.KotlinLogging
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import java.time.Instant.now

@SpringBootTest
class DemoAppITest {

    private val logger = KotlinLogging.logger {}

    @Autowired
    private lateinit var dataCommands: DataCommands
    @Autowired
    private lateinit var tagCommands: TagCommands

    @Test
    fun loadContext() { }

    @Test
    fun testDbData() {
        val name = "test${now().nano}"
        dataCommands.rmData(name)
        Assertions.assertTrue(!dataCommands.listData().contains(name))
        Assertions.assertEquals("Data $name added", dataCommands.addData(name, "desc"))
        Assertions.assertTrue(dataCommands.listData().contains(name))
        logger.info { dataCommands.showData(name) }
        Assertions.assertEquals("Data $name removed", dataCommands.rmData(name))
        Assertions.assertEquals("Data $name not exists", dataCommands.rmData(name))
    }

    @Test
    fun testDbTags() {
        val name = "test${now().nano}"
        dataCommands.rmData(name)
        dataCommands.addData(name, "desc")
        Assertions.assertEquals("", tagCommands.listTagsForData(name))
        Assertions.assertEquals("Tag k1=v1 added for data $name", tagCommands.addTagForData(name, "k1", "v1"))
        Assertions.assertEquals("Tag k1 already exists for data $name", tagCommands.addTagForData(name, "k1", "v2"))
        Assertions.assertEquals("Tag k2=v1 added for data $name", tagCommands.addTagForData(name, "k2", "v1"))
        Assertions.assertEquals("k1 = v1\nk2 = v1", tagCommands.listTagsForData(name))
        Assertions.assertEquals("Tag k1 removed for data $name", tagCommands.removeTagForData(name, "k1"))
        Assertions.assertEquals("k2 = v1", tagCommands.listTagsForData(name))
        dataCommands.rmData(name)
        Assertions.assertEquals("", tagCommands.listTagsForData(name))
    }
}
