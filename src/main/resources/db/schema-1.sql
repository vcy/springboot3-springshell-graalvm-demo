PRAGMA foreign_keys = ON;

CREATE TABLE IF NOT EXISTS data(
    name VARCHAR PRIMARY KEY,
    description TEXT
);

CREATE TABLE IF NOT EXISTS data_tag (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    dataName VARCHAR not null,
    key TEXT not null,
    value TEXT not null,
    FOREIGN KEY(dataName) REFERENCES data(name) ON DELETE CASCADE
);

CREATE UNIQUE INDEX IF NOT EXISTS idx_data_tag_dataName_key
    ON data_tag (dataName, key);
