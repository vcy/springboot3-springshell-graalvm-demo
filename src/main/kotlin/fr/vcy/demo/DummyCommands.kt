package fr.vcy.demo

import org.springframework.shell.component.flow.ComponentFlow
import org.springframework.shell.standard.ShellCommandGroup
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod


@ShellCommandGroup("Dummy Commands")
@ShellComponent
class DummyCommands(
    private val componentFlowBuilder: ComponentFlow.Builder
) {
    @ShellMethod("say hello.")
    fun hello(you: String) = "hello $you"

    @ShellMethod("demo")
    fun demo(): String {
        val result = componentFlowBuilder.clone().reset()
            .withStringInput("name")
            .name("Name")
            .defaultValue("toto")
            .and()
            .withSingleItemSelector("Are you 18 or older ?")
            .name("Age")
            .selectItems(mapOf("I'm 18 or older" to "18+", "I'm just a child" to "18-"))
            .next { ctx -> if (ctx.resultItem.get().item == "18+") "Confirm" else null }
            .and()
            .withConfirmationInput("Confirm")
            .name("You really want to do this?")
            .and()
            .build()
            .run()
        return if (runCatching { result.context.get<Boolean>("Confirm") == true }.getOrDefault(false)) {
            " => Welcome in"
        } else {
            " => Grow up ${result.context.get<String>("name")}"
        }
    }
}
