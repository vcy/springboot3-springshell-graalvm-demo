package fr.vcy.demo

import org.jooq.DSLContext
import org.springframework.shell.standard.ShellCommandGroup
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod

@ShellCommandGroup("Catalog Commands")
@ShellComponent
class TagCommands(
    private val dsl: DSLContext
) {

    @ShellMethod(value = "List all tags for data.", key = ["tag list"])
    fun listTagsForData(data: String) = dsl.fetch("SELECT key, value FROM data_tag WHERE dataName = ?", data)
        .map { record -> "${record.get("key", String::class.java)} = ${record.get("value", String::class.java)}" }
        .joinToString("\n")

    @ShellMethod(value = "Add tag for data.", key = ["tag add"])
    fun addTagForData(data: String, key: String, value: String): String {
        val res = dsl.execute("INSERT INTO data_tag (dataName, key, value) VALUES (?, ?, ?) ON CONFLICT DO NOTHING", data, key, value)
        return if (res > 0) { "Tag $key=$value added for data $data" } else "Tag $key already exists for data $data"
    }

    @ShellMethod(value = "Remove tag for data.", key = ["tag rm"])
    fun removeTagForData(data: String, key: String): String {
        val res = dsl.execute("DELETE FROM data_tag where dataName = ? and key = ?", data, key)
        return if (res > 0) { "Tag $key removed for data $data" } else "Tag $key not exists for data $data"
    }

}
