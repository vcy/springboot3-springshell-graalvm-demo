package fr.vcy.demo

import org.jline.utils.AttributedString
import org.jline.utils.AttributedStyle
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.shell.jline.PromptProvider


fun main(args: Array<String>) {
    runApplication<DemoApp>(*args)
}

@SpringBootApplication
class DemoApp {
    @Bean
    fun prompt() = PromptProvider { AttributedString("vcy> ", AttributedStyle.DEFAULT.foreground(AttributedStyle.GREEN)) }
}
