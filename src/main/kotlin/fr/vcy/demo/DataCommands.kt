package fr.vcy.demo

import org.jooq.DSLContext
import org.springframework.shell.standard.ShellCommandGroup
import org.springframework.shell.standard.ShellComponent
import org.springframework.shell.standard.ShellMethod


@ShellCommandGroup("Catalog Commands")
@ShellComponent
class DataCommands(
    private val dsl: DSLContext
) {

    @ShellMethod(value = "List all data.", key = ["data ls"])
    fun listData() = dsl.fetch("SELECT name FROM data")
        .into(String::class.java)
        .joinToString("\n")

    @ShellMethod(value = "Create new data.", key = ["data add"])
    fun addData(name: String, description: String?): String {
        dsl.execute("INSERT INTO data (name, description) VALUES (?, ?)", name, description)
        return "Data $name added"
    }

    @ShellMethod(value = "Delete data.", key = ["data rm"])
    fun rmData(name: String): String {
        val res = dsl.execute("DELETE FROM data where name = ?", name)
        return if (res > 0) { "Data $name removed" } else "Data $name not exists"
    }

    @ShellMethod(value = "Show data.", key = ["data show"])
    fun showData(name: String) = dsl.fetch("SELECT * FROM data WHERE name = ?", name)
        .map { record -> record.intoMap().filterKeys { it != "id" }.filterValues { it != null }.map { (t, u) -> "$t = $u" }.joinToString("\n") }
        .joinToString("\n")

}
